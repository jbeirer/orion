#!/usr/bin/env python3

from distributed import Client
from orion.client import build_experiment
from dask_jobqueue import HTCondorCluster
from orion.executor.base import executor_factory
import numpy as np
import subprocess
import socket
import dask
import os
import sys
import time

####### GLOBAL CONFIGURATION ########
   
# Change default HTCondor scheduler
os.environ['_condor_SCHEDD_HOST'] = 'bigbird17.cern.ch'
os.environ['_condor_CREDD_HOST']  = 'bigbird17.cern.ch'


global N_MODEL_PARAMETERS
N_MODEL_PARAMETERS = 5

####### GLOBAL CONFIGURATION ########


# Main function to minimize with Orion on the batch nodes
def main(parA, parB, parC, parD, parE = 1, intE = 65536, intMinEta = 20, pdgId = 22, layer = 1, nEvents=1000):

    import subprocess
    import shlex
    import os
    import sys 
    import logging
    import math


    logLevel = logging.INFO # Set logging.DEBUG in order to activate all logging information 

    # Configure format of logger and add handler to output to stdout so that HTCondor will catch in logfiles
    logging.basicConfig(format='%(asctime)s %(name)s %(levelname)s %(message)s', datefmt='%H:%M:%S',level=logLevel, stream=sys.stdout)

    logging.info('Initializing error computation for suggeted parameters...')

    modelParameterList = [parA, parB, parC, parD, parE]

    modelParameterListStr = ' '.join([str(i) for i in modelParameterList])
        

    # Unique string to identify job output
    simID = f"pid{pdgId}_E{intE}_eta_{intMinEta}_{intMinEta + 5}_parScan_{'_'.join([str(i) for i in modelParameterList])}"

    logging.info(f'Unique simID: {simID}')

    # List of variables to be considered
    variableList = ['weta1', 'wtots1', 'fracs1', 'eratio']

    # Ensure the working directory is correct
    condorWorkDir = os.environ.get("_CONDOR_SCRATCH_DIR")

    # Go to working directory
    os.chdir(condorWorkDir)

    # Define importance weights
    weights = {'weta1' : 1, 'wtots1': 1, 'fracs1': 1, 'eratio' : 1}

    # Define the command to run the shape validation
    cmd = f'./script.sh {pdgId} {nEvents} {intE} {intMinEta/100} {layer} {modelParameterListStr} {simID}'

    # Log script command
    logging.info(f'Script command: {cmd}')

    try:

        # Run the shape validation with the given parameters
        with subprocess.Popen(cmd, shell=True) as p:
            out, err = p.communicate()

        logging.info('Command done')    
        # Read the output files from the shape validation  
        fileList = [f'{var}_data_{simID}.txt' for var in variableList]  

        logging.info(f'fileList {fileList}')

        data = readFiles(fileList, os.getcwd() + '/')

        logging.info(f'data {data}')

        # Read the means from the data as a dictionary
        peakMean = getMeans(modelParameterList, data, variableList)

        # Get the target configuration of the shower shape means
        baseLineMean, baseLineRMS, percentageIncrease = getTargetConfig(intE, intMinEta, layer)

        # Extract the target means from the percentage increase and save as dictionary
        targetMean = {var : baseLineMean[var] + baseLineMean[var]*percentageIncrease[var]/100 for var in variableList}

        # Log target and computed means
        logging.info(f'Computed means: {peakMean}\nTarget means: {targetMean}')

        # Compute the error as a chi**2 like quantity with the means only
        error_rate = sum([weights[var]*((peakMean[var] - targetMean[var]) / baseLineRMS[var])**2 / len(variableList) for var in variableList])
        
        # Compute the average baseline error 
        baseline_error_rate = sum([weights[var]*((baseLineMean[var] - targetMean[var]) / baseLineRMS[var])**2 / len(variableList) for var in variableList])

        # Log baseline and computed error rate
        logging.info(f'Error rate: {error_rate}\nBaseline error rate: {baseline_error_rate}\nWeights: {weights}')

        # Make sure error rate is a sensible number
        if(math.isinf(error_rate) or math.isnan(error_rate)): 
            error_rate = 999999
            logging.warning(f'Error rate is not a number: {error_rate}')


        # Gather meta data that wwill be saved as additional statistic of each trial
        meta_data = {   
                        'intE' : intE, 'intMinEta' : intMinEta, 'pdgId' : pdgId, 
                        'layer' : layer, 'nEvents' : nEvents,
                        'baseLineMean' : baseLineMean, 'baseLineRMS' : baseLineRMS,
                        'percentageIncrease' : percentageIncrease, 
                        'targetMean' : targetMean, 'blackBoxMean' : peakMean, 
                        'baseline_error_rate' : baseline_error_rate
                    }

        return [
                    {"name": "meta_data", "type": "statistic", "value": meta_data}, 
                    {"name": "error",     "type": "objective", "value": error_rate}
                ]

    # Return large error rate if something failed
    except:
        logging.info(f'Compute failed for parameters: {modelParameterList}\nsimID: {simID}')
        return [{"name": "error", "type": "objective", "value": 99999999}]


def getTargetConfig(intE, intMinEta, layer, pathPrefix = ''):

    import json

    # File holding the scaling percentages by which variable means need to be scaled
    with open(f'{pathPrefix}scalingPercentages.json') as json_file:
        scalingData = json.load(json_file)

    # File holding the nominal variable means computed with the standalone simulation
    with open(f'{pathPrefix}means_pid22_layer{layer}.json') as json_file:
        meanData = json.load(json_file)

    # File holding the nominal variable RMS values computed with the standalone simultion
    with open(f'{pathPrefix}rms_pid22_layer{layer}.json') as json_file:
        RMSData = json.load(json_file)

    # Target configuration
    targetConfig = {

        "percentageIncrease" : scalingData[str(intE)][str(intMinEta)],

        "baseLineMean" :       meanData[str(intE)][str(intMinEta)],

        "baseLineRMS"  :       RMSData[str(intE)][str(intMinEta)]

    }

    return targetConfig['baseLineMean'], targetConfig['baseLineRMS'], targetConfig['percentageIncrease']


# Method to read contents of files specified in fileList that are stored in path
def readFiles(fileList, path):
    import os
    
    data = []
    for file in fileList:
        # Open file if output exists, if not, keep watching
        if os.path.isfile(path + file):
            f = open(path + file, 'r')
        else:
            raise Exception(f'Output file {file} not found in {path}. Files in directory: {os.listdir()}')
        content = f.read()
        f.close()
        data.append(content)
    return data

# Method to read meanList from data returned by readFiles method
def getMeans(modelParameterList, data, variableList):
    import math 

    meanDict = {}
    featureIdx = 0
    for feature in range(0, len(data)):

        featureData = [float(i.strip('\n')) for i in data[feature].split(',')]
        idx = 0
        for modelPar in modelParameterList:
            if not math.isclose(modelPar, featureData[idx], rel_tol=1e-5):
                raise Exception(f"The requested model parameter ({modelPar}) does not seem to fit the evaluated parameter ({featureData[idx]})")
            idx = idx + 1
        mean = featureData[N_MODEL_PARAMETERS]
        meanDict[variableList[featureIdx]] = mean
        featureIdx = featureIdx + 1
    return meanDict


def configureExperiment(name, nTrials, branchName = None):
    

    # Specify the database where the experiments are stored. We use mongoDB on a CERN VM to have access from the batch nodes
    storage = {
        "database": {
            "type": "mongodb",
            "name": "orion",
            "host": "mongodb://jbeirer-vm001",
            "port": "27017"
        },
    }


    algorithms = {

        
        "random": 
                {
                    "seed" : 42
                }
                
        
        # "tpe": 
        #     {
        #         "seed" : 42,
        #         "n_initial_points" : 20,
        #         "n_ei_candidates"  : 25,
        #         "gamma" : 0.25,
        #         "equal_weight" : False,
        #         "prior_weight" : 1.0,
        #         "full_weight_num" : 25
        #     } 

        # "RoBO_GP":
        #     {
        #         "seed": 42,
        #         "n_initial_points": 20,
        #         "maximizer": 'random',
        #         "acquisition_func": 'log_ei',
        #         "normalize_input": True,
        #         "normalize_output": False
        #     }
    }
    



    # Specify the search space


    # nominal search space ("devSearchSpace4") tuned for layer 1, eta=0.20, E655365 --> should be use for eta ⋹ [0, 0.7]
    # space = {

    #     "parA": "uniform(0.10, 0.35)",
    #     "parB": "uniform(0.2, 0.5)",
    #     "parC": "uniform(-0.6, -0.3)",
    #     "parD": "uniform(0.2, 0.4)"
    # }

    # Search space tuned for layer 1, eta=1.0, E655365 --> should be use for eta ⋹ [0.7, 1.4]
    space = {

            "parA": "uniform(0.7, 0.9)",
            "parB": "uniform(-0.1, 0.1)",
            "parC": "uniform(-0.4, -0.2)",
            "parD": "uniform(0.2, 0.4)"
    }

    # Seeding search space
    # space = {

    #         "parA": "uniform(-3, 3)",
    #         "parB": "uniform(-1, 1)",
    #         "parC": "uniform(-1, 1)",
    #         "parD": "uniform(-1, 1)"
    # }

    # Load the data for the specified experiment
    experiment = build_experiment(
        name        = name,
        branching   = {"branch_from": branchName},
        max_trials  = nTrials,
        space       = space,
        storage     = storage,
        algorithms  = algorithms,
        max_broken  = 9999
    )

    return experiment

def configureCluster(clusterName):

    import subprocess
    import glob

    # Directory that holds all files that will be transferred to batch nodes
    inputFileDir = 'files'
    inputFileList = [file for file in glob.glob(inputFileDir + '/*')]

    # Create a directory for the log files
    subprocess.call(['mkdir', '-p', f'logs/{clusterName}'])

    # Fixed cluster port reserved to using dask on lxplus nodes
    n_port = 8786

    print("############################################################")
    print(f'Scheduler port: port : {n_port}')
    print(f'Scheduler host: host : {socket.gethostname()}')
    print("############################################################")

    cluster = HTCondorCluster(
                cores=1,
                memory='2000MB',
                disk='1000MB',
                death_timeout = '3600', #allow up to an hour to submit all jobs to condor
                nanny = False,
                silence_logs = 'info',
                log_directory = '/afs/cern.ch/user/j/jbeirer/FastCaloSim/FCS-data-tuning/Orion/',
                scheduler_options={
                    'port': n_port,
                    'host': socket.gethostname()
                },
                job_extra={
                    'log':     f'logs/{clusterName}/job.log.$(Cluster)',
                    'output':  f'logs/{clusterName}/job.out.$(Cluster)-$(Process)',
                    'error':   f'logs/{clusterName}/job.err.$(Cluster)-$(Process)',
                    'should_transfer_files': 'YES',
                    'when_to_transfer_output': 'ON_EXIT',
                    'transfer_input_files': ','.join(inputFileList),
                    'arguments': 'in1 in2 out1',
                    '+TransferOutput' : '', # Do not transfer any output
                    '+JobFlavour': '"nextweek"', # when jobs will be terminated if not done, needs to have format '"longlunch"' !!! With longlunch jobs are not allowed to run longer than 2h
                    '+AccountingGroup': '"group_u_ATLAST3.all"', 
                },
                extra = ['--worker-port 10000:10100']
            )

    return cluster


if __name__ == '__main__':

  
    # Set the number of trials
    nTrials = 2000
    #Set the number of backend (HTCondor) workers that should run in parallel
    nWorkers = 200

    #### Configuration ####

    # intEnergyList = [32768, 65536, 131072, 262144, 524288, 1048576]
    # intMinEtaList = np.arange(0, 250, 5)
    intEnergyList = [131072]
    intMinEtaList = [200]
    layer = 5
    pdgId = 22
    nEvents = 1000

    #### Configuration ####


    cluster = configureCluster('cluster')
    client = Client(cluster) 
    cluster.scale(nWorkers)


    # Encapsulate experiment creation to run all in parallel
    def exec_experiment(executor, nWorkers, experiment, kwargs):
        experiment.executor = executor
        experiment.workon(main, n_workers = nWorkers,  **kwargs)

    # Create the dask executor
    executor = executor_factory.create("dask", n_workers=nWorkers, client=client)

    # List holding the expermine
    exp_futures = []

    for intE in intEnergyList:

        for intMinEta in intMinEtaList:

            # Constant keyword arguments to be passed to the function to be minimized
            kwargs = {'intE': intE, 'intMinEta': intMinEta, 'pdgId' : pdgId, 'layer' : layer, 'nEvents' : nEvents}

            # Check if target config is available, skip if we don't have the required data
            try:
                getTargetConfig(intE, intMinEta, layer, pathPrefix = 'files/')
            except:
                print(f'Target config unavailable. Skipping E = {intE}, minEta = {intMinEta}.')
                continue

            #experimentName = f'etaScaling_polynomialSeriesDeg3_randomSearch_devSearchSpace_4_E{intE}_etaMin_{intMinEta}'
            experimentName = f'etaScaling_polynomialSeriesDeg3_randomSearch_forwardAnalysis_v8_E{intE}_etaMin_{intMinEta}_layer{layer}'

            branchName = None
            experiment = configureExperiment(experimentName, nTrials, branchName)

            exp_futures.append(client.submit(exec_experiment, executor, nWorkers, experiment, kwargs))

    for exp_future in exp_futures:
        # Wait until experiments are complete
        client.gather(exp_future)





