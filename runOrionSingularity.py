#!/usr/bin/env python3

from distributed import Client
from orion.client import build_experiment
from dask_lxplus import CernCluster
from orion.executor.base import executor_factory
import numpy as np
import subprocess
import socket
import dask
import dask.distributed
import os
import sys
import time
import json
import traceback


####### GLOBAL CONFIGURATION ########
   
# Change default HTCondor scheduler
os.environ['_condor_SCHEDD_HOST'] = 'bigbird17.cern.ch'
os.environ['_condor_CREDD_HOST']  = 'bigbird17.cern.ch'


global N_MODEL_PARAMETERS
N_MODEL_PARAMETERS = 5

####### GLOBAL CONFIGURATION ########


# Main function to minimize with Orion on the batch nodes
def main(parA, parB, parC, parD, parE = 1, intE = 65536, intMinEta = 20, pdgId = 22, layer = 1, nEvents=1000):

    import subprocess
    import shlex
    import os
    import sys 
    import logging
    import math


    logLevel = logging.INFO # Set logging.DEBUG in order to activate all logging information 

    # Configure format of logger and add handler to output to stdout so that HTCondor will catch in logfiles
    logging.basicConfig(format='%(asctime)s %(name)s %(levelname)s %(message)s', datefmt='%H:%M:%S',level=logLevel, stream=sys.stdout)

    logging.info('Initializing error computation for suggeted parameters...')

    modelParameterList = [parA, parB, parC, parD, parE]

    modelParameterListStr = ' '.join([str(i) for i in modelParameterList])
        
    # Unique string to identify job output
    simID = f"pid{pdgId}_E{intE}_eta_{intMinEta}_{intMinEta + 5}_parScan_{'_'.join([str(i) for i in modelParameterList])}"

    logging.info(f'Unique simID: {simID}')

    # List of variables to be considered
    variableList = ['weta1', 'wtots1', 'fracs1', 'eratio']

    # Ensure the working directory is correct
    condorWorkDir = os.environ.get("_CONDOR_SCRATCH_DIR")

    # Go to working directory
    os.chdir(condorWorkDir)

    # Define importance weights
    weights = {'weta1' : 1, 'wtots1': 1, 'fracs1': 1, 'eratio' : 1}

    # Define the command to run the shape validation
    cmd = f'./script.sh {pdgId} {nEvents} {intE} {intMinEta/100} {layer} {modelParameterListStr} {simID}'

    # Log script command
    logging.info(f'Script command: {cmd}')

    try:

        # Run the shape validation with the given parameters
        with subprocess.Popen(cmd, shell=True) as p:
            out, err = p.communicate()

        logging.info('Command done')    
        # Read the output files from the shape validation  
        fileList = [f'{var}_data_{simID}.txt' for var in variableList]  

        logging.info(f'fileList {fileList}')

        data = readFiles(fileList, os.getcwd() + '/')

        logging.info(f'data {data}')

        # Read the means from the data as a dictionary
        peakMean = getMeans(modelParameterList, data, variableList)

        # Get the target configuration of the shower shape means
        baseLineMean, baseLineRMS, percentageIncrease = getTargetConfig(intE, intMinEta, layer)

        # Extract the target means from the percentage increase and save as dictionary
        targetMean = {var : baseLineMean[var] + baseLineMean[var]*percentageIncrease[var]/100 for var in variableList}

        # Log target and computed means
        logging.info(f'Computed means: {peakMean}\nTarget means: {targetMean}')

        # Compute the error as a chi**2 like quantity with the means only
        error_rate = sum([weights[var]*((peakMean[var] - targetMean[var]) / baseLineRMS[var])**2 / len(variableList) for var in variableList])
        
        # Compute the average baseline error 
        baseline_error_rate = sum([weights[var]*((baseLineMean[var] - targetMean[var]) / baseLineRMS[var])**2 / len(variableList) for var in variableList])

        # Log baseline and computed error rate
        logging.info(f'Error rate: {error_rate}\nBaseline error rate: {baseline_error_rate}\nWeights: {weights}')

        # Make sure error rate is a sensible number
        if(math.isinf(error_rate) or math.isnan(error_rate)): 
            error_rate = 999999
            logging.warning(f'Error rate is not a number: {error_rate}')


        # Gather meta data that wwill be saved as additional statistic of each trial
        meta_data = {   
            'intE' : intE, 'intMinEta' : intMinEta, 'pdgId' : pdgId, 
            'layer' : layer, 'nEvents' : nEvents,
            'baseLineMean' : baseLineMean, 'baseLineRMS' : baseLineRMS,
            'percentageIncrease' : percentageIncrease, 
            'targetMean' : targetMean, 'blackBoxMean' : peakMean, 
            'baseline_error_rate' : baseline_error_rate
        }

        return [
            {"name": "meta_data", "type": "statistic", "value": meta_data}, 
            {"name": "error",     "type": "objective", "value": error_rate}
        ]

        # Return large error rate if something failed
    
    except:
        
        logging.info(f'Compute failed for parameters: {modelParameterList}\nsimID: {simID}')
        return [{"name": "error", "type": "objective", "value": 99999999}]

def getTargetConfig(intE, intMinEta, layer, pathPrefix = ''):

    import json

    # File holding the scaling percentages by which variable means need to be scaled
    with open(f'{pathPrefix}scalingPercentages.json') as json_file:
        scalingData = json.load(json_file)

    # File holding the nominal variable means computed with the standalone simulation
    with open(f'{pathPrefix}means_pid22_layer{layer}.json') as json_file:
        meanData = json.load(json_file)

    # File holding the nominal variable RMS values computed with the standalone simultion
    with open(f'{pathPrefix}rms_pid22_layer{layer}.json') as json_file:
        RMSData = json.load(json_file)

    # Target configuration
    targetConfig = {

        "percentageIncrease" : scalingData[str(intE)][str(intMinEta)],

        "baseLineMean" :       meanData[str(intE)][str(intMinEta)],

        "baseLineRMS"  :       RMSData[str(intE)][str(intMinEta)]

    }

    return targetConfig['baseLineMean'], targetConfig['baseLineRMS'], targetConfig['percentageIncrease']


# Method to read contents of files specified in fileList that are stored in path
def readFiles(fileList, path):
    import os
    
    data = []
    for file in fileList:
        # Open file if output exists, if not, keep watching
        if os.path.isfile(path + file):
            f = open(path + file, 'r')
        else:
            raise Exception(f'Output file {file} not found in {path}. Files in directory: {os.listdir()}')
        content = f.read()
        f.close()
        data.append(content)
    return data

# Method to read meanList from data returned by readFiles method
def getMeans(modelParameterList, data, variableList):
    import math 

    meanDict = {}
    featureIdx = 0
    for feature in range(0, len(data)):

        featureData = [float(i.strip('\n')) for i in data[feature].split(',')]
        idx = 0
        for modelPar in modelParameterList:
            if not math.isclose(modelPar, featureData[idx], rel_tol=1e-5):
                raise Exception(f"The requested model parameter ({modelPar}) does not seem to fit the evaluated parameter ({featureData[idx]})")
            idx = idx + 1
        mean = featureData[N_MODEL_PARAMETERS]
        meanDict[variableList[featureIdx]] = mean
        featureIdx = featureIdx + 1
    return meanDict


def configureExperiment(name, nMaxTrials, space, algorithm, branchName = None):
    

    # Specify the database where the experiments are stored. We use mongoDB on a CERN VM to have access from the batch nodes
    storage = {
        "database": {
            "type": "mongodb",
            "name": "orion",
            "host": "mongodb://jbeirer-vm001",
            "port": "27017"
        },
    }


    # Load the data for the specified experiment
    experiment = build_experiment(
        name        = name,
        branching   = {"branch_from": branchName},
        max_trials  = nMaxTrials,
        space       = space,
        storage     = storage,
        algorithms  = algorithm,
        max_broken  = 9999
    )

    return experiment


def configureCluster():

    import subprocess
    import glob

    # Directory that holds all files that will be transferred to batch nodes
    inputFileDir = '/afs/cern.ch/user/j/jbeirer/FastCaloSim/FCS-data-tuning/Orion/files'
    inputFileList = [file for file in glob.glob(inputFileDir + '/*')]
    # Create a directory for the log files
    logDirectoryPath = 'logs'
    #subprocess.call(['mkdir', '-p', logDirectoryPath])

    # Fixed cluster port reserved to using dask on lxplus nodes
    n_port = 8786

    print("############################################################")
    print(f'Scheduler port: port : {n_port}')
    print(f'Scheduler host: host : {socket.gethostname()}')
    print("############################################################")

    cluster = CernCluster(
                cores=1,
                memory='2000MB',
                disk='1000MB',
                death_timeout = '3600', #allow up to an hour to submit all jobs to condor
                nanny = False,
                image_type = "singularity",
                batch_name = "orion",
                worker_image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/batch-team/dask-lxplus/lxdask-cc7:latest",
                silence_logs = 'info',
                log_directory = logDirectoryPath,
                scheduler_options={
                    'port': n_port,
                    'host': socket.gethostname()
                },
                job_extra={
                    'log':     f'{logDirectoryPath}/job.log.$(Cluster)',
                    'output':  f'{logDirectoryPath}/job.out.$(Cluster)-$(Process)',
                    'error':   f'{logDirectoryPath}/job.err.$(Cluster)-$(Process)',
                    'should_transfer_files': 'YES',
                    'when_to_transfer_output': 'ON_EXIT',
                    'transfer_input_files': ','.join(inputFileList),
                    'transfer_output_files': ',',
                    'arguments': 'in1 in2 out1',
                    '+TransferOutput' : '',      # Do not transfer any output (including log files)
                    '+JobFlavour': '"nextweek"', # Allow jobs to run for a long time 
                    '+AccountingGroup': '"group_u_ATLAST3.all"', 
                },
            )

    return cluster


class experimentManager:
    """
    Class used to manage the execution of experiments on the HTCondor cluster using dask

    ...

    Attributes
    ----------
    nWorkers : int
        Number of HTCondor workers to be submitted
    client : Dask client
    exp_futures : list
        List of current dask futures of experiments that the manager holds 
    executor : Orion executor
        Orion executor using Dask as backend
    exe_experiment: fun
        Encapsulated experiment execution method

    """
    def __init__(self, nWorkers, client, cluster, exec_experiment):
        self.nWorkers = nWorkers
        self.client = client
        self.cluster = cluster
        self.exp_futures = []
        self.executor = self._createExecutor(nWorkers, client)
        self.exec_experiment = exec_experiment
        self.nReleases = 0

        # Value of 1000 means we will not parallelize experiments (for 500 or more workers)
        # Can also directly limits number of max experiments
        # Whatever will be fulfilled first will be used
        self.DBConnectionThreshold = 1000
        self.maxExperiments = 1

    def _createExecutor(self, nWorkers, client):
        # Create orion dask executor
        executor = executor_factory.create('dask', n_workers = nWorkers, client = client)
        return executor

    def _releaseExperiments(self):

        print(f'RUN.ORION - RELEASING {len(self.exp_futures)} EXPERIMENTS ({self.nReleases} total releases so far)')
        # Release the experiments and wait for results
        for exp_future in self.exp_futures:
            try:    
                self.client.gather(exp_future)
            except Exception:
                print(f'RUN.ORION - The following exception occurred:')
                traceback.print_exc()                
                print(f'RUN.ORION - TRYING TO RESTARTING CLIENT AND PROCEEDING WITH NEXT BATCH')
                # If something goes wrong, cancel all futures to safely go to the next experiment batch
                self.client.cancel(self.exp_futures)
                self.client.restart()
                self.cluster.scale(self.nWorkers)
                self.exp_futures = []
        # Clear list of experiment dask futures
        self.exp_futures = []
        self.nReleases += 1
    
    def _shouldRelease(self):

        # Release the experiments if the number of expected DB connections exceeds threshold
        nExpectedDBConnections = 2*self.nWorkers*len(self.exp_futures) + len(self.exp_futures)
        if nExpectedDBConnections > self.DBConnectionThreshold or len(self.exp_futures) == self.maxExperiments: return True 
        else: return False

    def addExperiment(self, experiment, kwargs):
        
        # Append experiment future to list
        exp_future = client.submit(self.exec_experiment, self.executor, self.nWorkers, experiment, kwargs)
        self.exp_futures.append(exp_future)

        # Release experiments if user set condition is fulfilled 
        if self._shouldRelease(): self._releaseExperiments()


if __name__ == '__main__':

    # Set the number of trials to be run per sub-experiment (eta - energy point)
    nMaxTrials = 1000
    #Set the total number of backend (HTCondor) workers that should run in parallel to process all submitted experiments
    nWorkers = 150
    # Use already completed trials from experiment
    usePreviousTrials = True

    # Configure cluster and client and scale to requested number of workers
    cluster = configureCluster()
    client  = Client(cluster) 
    cluster.scale(nWorkers)

    # Encapsulate experiment execution in method which we pass to the experiment manager
    def exec_experiment(executor, nWorkers, experiment, kwargs):
        experiment.executor = executor
        experiment.workon(main, n_workers = nWorkers,  **kwargs)

    # Create the experiment manager
    expManager = experimentManager(nWorkers, client, cluster, exec_experiment)

    # Load the configuration file
    with open('config/config.json') as json_file:
        config = json.load(json_file)

    # Loop over all experiments defined in the configuration file
    for exp in config.keys():
        # Loop over all energy and eta points set in the configuration file for particular experiment
        for intE in eval(config[exp]['intEnergyList']):
            for intMinEta in eval(config[exp]['intMinEtaList']):
                
                # Access required data from config file
                layer     = config[exp]['layer']
                pdgId     = config[exp]['pdgId']
                nEvents   = config[exp]['nEvents']
                space     = config[exp]['space']
                algorithm = config[exp]['algorithm']

                # Check if target config is available, skip if we don't have the required data
                try:
                    getTargetConfig(intE, intMinEta, layer, pathPrefix = 'files/')
                except:
                    print(f'Target config unavailable. Skipping E = {intE}, minEta = {intMinEta}, layer = {layer}, pdgId = {pdgId}')
                    continue

                # Sets the name of the experiments
                # with dirty hack to have identical keys in json, but keep original experiment naming
                expNameHack = exp.replace('_LAYER1', '').replace('_LAYER5', '')
                experimentName = f'{expNameHack}_E{intE}_etaMin_{intMinEta}_layer{layer}'

                # Configures if we want to use trials from previous runs of experiments
                if usePreviousTrials: branchName = experimentName
                else: branchName = None

                # Configure and build the Orion experiment
                experiment = configureExperiment(experimentName, nMaxTrials, space, algorithm, branchName)

                # Constant keyword arguments to be passed to the function to be minimized
                kwargs = {'intE': intE, 'intMinEta': intMinEta, 'pdgId' : pdgId, 'layer' : layer, 'nEvents' : nEvents}

                expManager.addExperiment(experiment, kwargs)

            




