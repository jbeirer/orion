"""
	Copyright (C) 2020-2022 Joshua Falco Beirer
	
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.
	
	"""


# Plotting imports
import matplotlib.pyplot as plt
import atlasify as atlas
import seaborn as sns

# Analysis imports
import pandas as pd
import numpy as np

# Orion imports
from orion.client import get_experiment
from orion.plotting.backend_plotly import _format_hyperparameters, _format_value
from orion.analysis import regret

# System imports
import os
import json


__version__ = '0.0.1'



def _setFigSize(length, height):
	plt.rcParams['figure.figsize'] = [length, height]

def _setup_mpl():
	_setFigSize(7, 5);
	plt.rcParams.update({'axes.labelsize': 12}) # set larger axis font for blind ATLAS authors


_setup_mpl()

def reload():
	importlib.reload


def getExperimentData(experimentName):
    '''Fetches an experiment from the database and returns a
    dictionary of the parameters from to the best trial and 
    the corresponding evaulation error'''
    # Get the experiment from the database
    experiment = get_experiment(experimentName)
    # Get ID of best experiment with minimum error
    bestTrialID = experiment.stats['best_trials_id']
    # Get best trial
    bestTrial   = experiment.get_trial(uid=bestTrialID)
    # Get best trial meta data
    meta_data   = bestTrial.results[0].value
    # Get evaluation error of best trial
    bestError   = bestTrial.results[1].value
    # Get optimal parameters
    bestPars    = bestTrial.params
    # Get number of completed trials
    completedTrials = experiment.stats['trials_completed']

    result = {'name' : experimentName, 
    		  'bestPars' : bestPars,
    		  'bestError': bestError,
    		  'completedTrials' : completedTrials
    		 }

    # Append any meta data to the final result dictionary
    result.update(meta_data)
    
    return result


def createDataframe(pathToConfig):

	with open(pathToConfig, encoding='utf-8') as json_file:
		config = json.load(json_file)

	dataFrameDict = {}

	# Loop over all experiments defined in the configuration file
	for exp in config.keys():
		
		# Create data frame for each experiment config
		df = pd.DataFrame(columns = config.keys())

		# Loop over all energy and eta points set in the configuration file for particular experiment
		for intE in eval(config[exp]['intEnergyList']):
			for intMinEta in eval(config[exp]['intMinEtaList']):

				layer = config[exp]['layer']

				# Generate experiment name
				experimentName = f'{exp}_E{intE}_etaMin_{intMinEta}_layer{layer}'

				try:
					# Query experiment results
					result = getExperimentData(experimentName)
					# Append results of experiments in dataframe
					df = df.append(result, ignore_index = True)
				except: 
					pass

		# Drop name of experiments in dataframe
		df = df.drop([exp, 'name'], axis = 1)
		# Append dataframe of experiment config to dataframe dict
		dataFrameDict[exp] = df

	return dataFrameDict
























