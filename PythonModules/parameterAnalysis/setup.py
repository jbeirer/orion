import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="parameterAnalysis", 
    version="0.0.1",
    author="Joshua Falco Beirer",
    author_email="jbeirer@cern.ch",
    description="Tool to extract and analyse Orion optimal parameters",
    long_description=long_description,
    long_description=load_long_description("README.md"),
    long_description_content_type='text/markdown',
    url="https://gitlab.cern.ch/jbeirer/parameterAnalysis",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: Physics"
    ],
    python_requires='>=3.6',
)