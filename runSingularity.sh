HOME_DIR=/afs/cern.ch/user/j/jbeirer/FastCaloSim/FCS-data-tuning/Orion


# Increase all kernel limits to maximum allowed to be sure
for opt in $(ulimit -a | sed 's/.*\-\([a-z]\)[^a-zA-Z].*$/\1/'); do
    ulimit -$opt unlimited 2>/dev/null
    ulimit -S$opt unlimited 2>/dev/null
done

# Get proper permissions
export KRB5CCNAME=FILE:/tmp/krb5cc_${UID}
kinit -c /tmp/krb5cc_${UID}
aklog


# Run the singularity container
singularity shell -H $HOME_DIR --contain --ipc  --bind /afs --bind /tmp/ --env KRB5CCNAME=$KRB5CCNAME /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/jbeirer/dask-lxbatch-orion/lxdask-orion-cc7:latest
