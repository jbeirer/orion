#!/bin/bash

PDGID=$1
nEVENTS=$2
INT_E=$3
ETA_MIN=$4
LAYER=$5
PAR_A=$6
PAR_B=$7
PAR_C=$8
PAR_D=$9
PAR_E=${10}
TUNING_OUTPUT_NAME=${11}

echo "PDGID : ${PDGID}"
echo "ETA_MIN : ${ETA_MIN}"
echo "INT_E : ${INT_E}"
echo "nEVENTS : ${nEVENTS}"
echo "LAYER : ${LAYER}"
echo "PAR_A : ${PAR_A}"
echo "PAR_B : ${PAR_B}"
echo "PAR_C : ${PAR_C}"
echo "PAR_D : ${PAR_D}"
echo "PAR_E : ${PAR_E}"
echo "TUNING_OUTPUT_NAME : ${TUNING_OUTPUT_NAME}"

# Setup ATLAS environment
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh ""
 
# Setup FastCaloSimV2 environment
lsetup "views LCG_94a x86_64-slc6-gcc62-opt"

# Untar file and source installation
tar zxvf install.tar.gz 
source install/usr/local/setup.sh

# Launch command 
runTFCSShapeValidation --pdgId ${PDGID} --energy ${INT_E} --etaMin ${ETA_MIN} --nEvents ${nEVENTS} --layer ${LAYER} --tuningParA ${PAR_A} --tuningParB ${PAR_B} --tuningParC ${PAR_C} --tuningParD ${PAR_D} --tuningParE ${PAR_E} --tuningOutputName ${TUNING_OUTPUT_NAME} 

rc=$?
echo "runTFCSShapeValidation ended with rc=$rc"
if [[ $rc != 0 ]]; then exit $rc; fi

exit $rc
